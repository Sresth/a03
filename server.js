var express = require('express');  // require express
var app = express();  // create a request handler function
var server = require('http').createServer(app);  // use our app to create a server
var io = require('socket.io')(server); // pass in our http app server to get a Socket.io server
var path = require('path');
var logger = require("morgan");
var bodyParser = require("body-parser");
 
// on a GET request to default page, do this.... 
app.get('/', function(req, res){
  app.use(express.static(path.join(__dirname)));
  res.sendFile(path.join(__dirname, '../A03Malpani/assets', 'Malpani_Sresth.html'));
});

app.get('/Malpani_Sresth', function(req, res){
  app.use(express.static(path.join(__dirname)));
  res.sendFile(path.join(__dirname, '../A03Malpani/assets', 'Malpani_Sresth.html'));
});

app.get('/MyChoice', function(req, res){
  app.use(express.static(path.join(__dirname)));
  res.sendFile(path.join(__dirname, '../A03Malpani/assets', 'MyChoice.html'));
});

app.get('/Contact', function(req, res){
  app.use(express.static(path.join(__dirname)));
  res.sendFile(path.join(__dirname, '../A03Malpani/assets', 'Contact.html'));
});



app.use(express.static(__dirname + '/assets'));

// set up the view engine
app.set("views", path.resolve(__dirname, "views")); // path to views
app.set("view engine", "ejs"); // specify our view engine

// manage our entries
var entries = [];
app.locals.entries = entries;

// set up the logger
app.use(logger("dev"));
app.use(bodyParser.urlencoded({ extended: false }));

// http GET (default and /new-entry)
app.get("/index", function (request, response) {
  response.render("index");
});
app.get("/new-entry", function (request, response) {
  response.render("new-entry");
});

// http POST (INSERT)
app.post("/new-entry", function (request, response) {
  if (!request.body.title || !request.body.body) {
    response.status(400).send("Entries must have a title and a body.");
    return;
  }
  entries.push({
    title: request.body.title,
    content: request.body.body,
    published: new Date()
  });
  response.redirect("/index");
});



//app.get('/guestbook', function(req, res){
 // app.use(express.static(path.join(__dirname)));
 // res.sendFile(path.join(__dirname, '../A03Malpani/assets', 'Thanks.html'));
//});
 
// on a connection event, act as follows (socket interacts with client)
io.on('connection', function(socket){ 
  socket.on('chatMessage', function(from, msg){  // on getting a chatMessage event
    io.emit('chatMessage', from, msg);  // emit it to all connected clients
  });
  socket.on('notifyUser', function(user){  // on getting a notifyUser event
    io.emit('notifyUser', user);  // emit to all 
  });
}); 
 
// Listen for an app request on port 8081
server.listen(8081, function(){
  console.log('listening on http://127.0.0.1:8081/');
});